# Weather Collection

## Description

Collects weather data from Metconnect New Zealand and outputs it as csv files. It does this by automating a browser.

Note: Get written permission from Metconnect before using
## Installation

To install the GUI application download Weather_Data_Download_Tool.exe from the above files. 

Alternatively follow the below inststructions to install the source code.


Requires Python. If you don't have Python installed you can install it [here](https://www.python.org/downloads/). This also requires the use of some Python packages.

You will need to install:

- selenium
- webdriver_manager
- tqdm

To do this open your command line and enter the command:
```
pip install selenium
```
Repeat with the other packages.


## Usage

Import the daily weather_data_funcs file, and then call the function `get_weather_stations`. Pass in your parameters, namely:

- List of weather station names
- The current year
- The number of years back from the current year to collect from
- Metconnect username
- Metconnect password
- Temporary path for downloading individual files (delected upon completion)
- Output path for completed csv files to be found

Valid weather station names can be found in the `weather_stations_lookup.py` file.

Here is a sample program:
```
import daily_weather_data_funcs

USERNAME = "123"
PASSWORD = "123"

CURRENT_YEAR = 2021
LOOK_BACK_YEAR = 5

DOWNLOAD_PATH = "Download"
OUTPUT_PATH = "Output"


# Leave blank for all weather stations
WEATHER_STATIONS = (
    "Waiouru North WXT",
    "Taupo WXT AWS",
)

daily_weather_data_funcs.get_weather_stations(
    WEATHER_STATIONS,
    CURRENT_YEAR,
    LOOK_BACK_YEAR,
    USERNAME,
    PASSWORD,
    DOWNLOAD_PATH,
    OUTPUT_PATH,
)
```

On completion, the output path should be populated with csv files containing weather data of the specified weather stations.

Source code can be compiled to an executable by using [pyinstaller](https://pyinstaller.org/en/stable/).
```
pyinstaller -F gui_program.py
```

## Support
For any queries or issues email

- Thomas.Whaley@powerco.co.nz
- Oliver.Silk@powerco.co.nz


