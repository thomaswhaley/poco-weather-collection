from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime


from webdriver_manager.chrome import ChromeDriverManager

from weather_stations_lookup import LOOKUP

import os

import time
from tqdm import tqdm

# Ignores leap years but I don't have time to deal with that
month_to_days = (-1,31,28,31,30,31,30,31,31,30,31,30,31)


def _legitemise_dir(directory: str) -> str:
    """Legitemises a directory checking if it exists etc

    Parameters
    ----------
    directory : str
        Directory to legitemise

    Returns
    -------
    str
        Legitemised directory
    """
    # Check if it is a local dir (eg. 'download', '/download', etc)
    if len([x for x in directory.split(os.path.sep) if x]) == 1:
        directory = f"{os.getcwd()}{os.path.sep}{directory}"
    # Create the directory if it doesn't exist
    if not os.path.exists(directory):
        os.mkdir(directory)
    return directory


def _clear_dir(directory: str) -> None:
    directory = _legitemise_dir(directory)

    for file in os.listdir(directory):
        if file.endswith(".csv"):
            os.remove(f"{directory}{os.path.sep}{file}")


def _format_date(day: int, month: int, year: int, delimiter: str = "/") -> str:
    return f"{day:0>2}{delimiter}{month:0>2}{delimiter}{year}"


def _get_chrome_driver(download_path: str) -> webdriver.Chrome:
    download_path = _legitemise_dir(download_path)
    options = webdriver.ChromeOptions()
    prefs = {}
    prefs["download.default_directory"] = download_path
    options.add_experimental_option("prefs", prefs)
    options.add_experimental_option("excludeSwitches", ["enable-logging"])

    options.headless = True
    options.add_argument('--window-size=1920,1080')  
    s = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=s, options=options)
    return driver


def _download_weather_data(
    driver: webdriver.Chrome,
    page: str,
    weather_station_id: str,
    year: int,
    month: int,
    day: int,
    download_path: str,
) -> str:
    # Clear the download directory
    _clear_dir(download_path)
    # Go to the website containing the weather data
    

    driver.get(
        f"https://nzta.metconnect.co.nz/mango/statistics/local/{weather_station_id}/{page}?date={year}{month:0>2}{day:0>2}#"
    )
    
    # Download data to csv
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="datatable_wrapper"]/div[1]/div/button[1]'))).click()

    # Wait for download
    start_time = time.time()
    while time.time() - start_time < 10:
        for file in os.listdir(download_path):
            if file.endswith('.csv') and os.path.isfile(download_path + os.path.sep + file) and os.access(download_path + os.path.sep + file, os.R_OK):
                time.sleep(0.05)
                return f"{download_path}{os.path.sep}{file}"

    raise ValueError('Timed out downloading file')

    

    # file = f"{download_path}{os.path.sep}{[file for file in os.listdir(download_path) if file.endswith('.csv')][0]}"
    # return file


def _weather_station(
    station_name: str,
    current_year: int,
    years: int,
    download_path: str,
    output_path: str,
    driver: webdriver.Chrome,
    hourly: bool
) -> None:
    # Check to see if the lookup table contains the given station name
    if not station_name in LOOKUP:
        print(
            f"Station {station_name} not found in lookup table. This is likely because it has been misspelled, or it doesn't appear on the Metconnect database."
        )
        return

    # Initialise a new chrome driver
    # driver = _get_chrome_driver(download_path)
    print(f"Downloading weather data from {station_name}")

    output_path = _legitemise_dir(output_path)

    # Get the data from the lookup table
    lat, long, station_id = LOOKUP[station_name]

    # Get the current date
    today = datetime.today()

    output_contents = ""

    # Create the progress bar
    if hourly:
        bar_size = 365 * years
    else:
        bar_size = 12 * years


    if current_year == today.year: monthRange = today.month
    else: monthRange = 12
    for month in range(1, monthRange + 1):
        if (hourly): bar_size += month_to_days[month]
        else: bar_size += 1

    progress_bar = tqdm(total=bar_size)

    


    for year in range(current_year - years, current_year + 1):
        if year == today.year: monthRange = today.month
        else: monthRange = 12
        for month in range(1, monthRange + 1):
            if hourly: 
                r = month_to_days[month]
                download_type = "daily"
            else:
                r = 1
                download_type = "monthly"
            for day in range(1, r+1):
                # Download weather data
                weather_file = _download_weather_data(
                    driver,
                    download_type,
                    station_id,
                    year,
                    month,
                    day,
                    download_path,
                )

                with open(weather_file, "r") as f:
                    file_contents = f.readlines()

                # Add csv headers
                if not len(output_contents):
                    output_contents += (
                        f"Date,{file_contents[0].strip()},Lat:,{lat},Long:,{long}\n"
                    )
                else:
                    output_contents += "\n"

                # Add the date to the start
                output_contents += "".join(
                    [
                        _format_date(day if hourly else i, month, year) + "," + line
                        for i, line in enumerate(file_contents)
                        if i != 0
                    ]
                )

                os.remove(weather_file)
                progress_bar.update(1)
    if hourly:
        suffix = "_hourly"
    else:
        suffix = "_daily"
    with open(f"{output_path}{os.path.sep}{station_name}{suffix}.csv", "w") as f:
        f.write(output_contents)
    
    progress_bar.close()
    print(f"Completed download of {station_name}")





def get_weather_stations(
    weather_stations: tuple,
    current_year: int,
    look_back_year: int,
    username: str,
    password: str,
    download_path: str,
    output_path: str,
    hourly: bool
) -> None:
    """Downloads weather data into csv files

    Parameters
    ----------
    weather_stations : tuple
        List of weather stations to download. Leave blank for all weather stations.
    current_year : int
        Year to start from.
    look_back_year : int
        Number of years back to collect data from.
    username : str
        Metconnect username
    password : str
        Metconnect password
    download_path : str
        Temporary path for downloading individual files (deleted at completion)
    output_path : str
        Path for completed csv files
    """
    start_time = time.time()
    # If left blank, do all
    if not len(weather_stations):
        weather_stations = tuple(LOOKUP.keys())
    if isinstance(weather_stations, str) == 1:
        weather_stations = (weather_stations,)

    driver = _get_chrome_driver(download_path)
    driver.get("https://nzta.metconnect.co.nz/mango/map/national")
    driver.find_element(By.XPATH, '//*[@id="username"]').send_keys(username)
    driver.find_element(By.XPATH, '//*[@id="password"]').send_keys(password)
    driver.find_element(By.XPATH, '//*[@id="proceed"]').click()
    try:
        for station_name in weather_stations:
            _weather_station(
                station_name,
                current_year,
                look_back_year,
                download_path,
                output_path,
                driver,
                hourly
            )
    except Exception:
        driver.find_element(By.XPATH, '//*[@id="logoutLink"]').click()
        driver.quit()
        _clear_dir(download_path)
        os.rmdir(download_path)
        raise

    # Logout
    driver.find_element(By.XPATH, '//*[@id="logoutLink"]').click()
    driver.quit()
    _clear_dir(download_path)
    os.rmdir(download_path)
    print("Retrieved data in", int(time.time() - start_time), "seconds.")
