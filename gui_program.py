import weather_data_funcs
import tkinter as tk

def run(stations,year,look_back,username,password,download_path,output_path,daily):
    weather_data_funcs.get_weather_stations(
        stations,
        year,
        look_back,
        username,
        password,
        download_path,
        output_path,
        daily
    )


DOWNLOAD_PATH = "Download"
OUTPUT_PATH = "Output"

root = tk.Tk()
root.geometry("425x475")
root.resizable(False, False)
root.title("Metconnect Weather Data Download Tool")

def validateNumber(S):
    if S in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        return True
    if type(S) == str and len(S) > 1:
        for c in S:
            if c not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                return False
        return True
    return False

vcmd = (root.register(validateNumber), '%S')


label = tk.Label(text = "Metconnect Weather Data Download Tool", font=("Segoe UI", 16))
label.pack()

usernameLabel = tk.Label(text = "Username", font=("Segoe UI", 12))
usernameLabel.pack()

usernameEntry = tk.Entry(width=35)
usernameEntry.pack()

passwordLabel = tk.Label(text = "Password", font=("Segoe UI", 12))
passwordLabel.pack()

passwordEntry = tk.Entry(show="*", width=35)
passwordEntry.pack()

stationsLabel = tk.Label(text = "Enter station names (1 per line, leave blank for all)", font=("Segoe UI", 12))
stationsLabel.pack()

stationsText = tk.Text(width=35,height=5)
stationsText.pack()

currentYearLabel = tk.Label(text = "Start Year", font=("Segoe UI", 12))
currentYearLabel.pack()

currentYearEntry = tk.Entry(validate="key", validatecommand=vcmd)
currentYearEntry.insert(tk.END, '2022')
currentYearEntry.pack()

lookBackLabel = tk.Label(text = "How many years to look back", font=("Segoe UI", 12))
lookBackLabel.pack()

lookBackEntry = tk.Entry(validate="key", validatecommand=vcmd)
lookBackEntry.insert(tk.END, '0')
lookBackEntry.pack()

daily = tk.BooleanVar()

dailyRadio = tk.Radiobutton(root, text="Daily", variable=daily, value=False, font=("Segoe UI", 12))
dailyRadio.pack()

hourlyRadio = tk.Radiobutton(root, text="Hourly", variable=daily, value=True, font=("Segoe UI", 12))
hourlyRadio.pack()


button = tk.Button(text = "Run", font=("Segoe UI", 12), command= lambda: run(
     stationsText.get(1.0, 'end-1c').split("\n"),
     int(currentYearEntry.get()),
     int(lookBackEntry.get()),
     usernameEntry.get(),
     passwordEntry.get(),
     DOWNLOAD_PATH,
     OUTPUT_PATH,
     daily.get()
    )
)
button.pack()

infoLabel = tk.Label(text = "Contact oliver.silk@powerco.co.nz with any questions", font=("Segoe UI", 12, "italic"))
infoLabel.pack()

root.mainloop()

